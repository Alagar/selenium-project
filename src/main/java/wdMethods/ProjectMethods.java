package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
public class ProjectMethods extends SeMethods {
	
	@BeforeSuite(groups = "all")
	public void startTest() {
		startResult();
	}
	@BeforeClass(groups = "all")
	public void before() {
		setBeforeData();
	}
	
	@Parameters({"url" , "username" , "password"})
	@BeforeMethod(groups = "all")
	public void login(String url,String username , String password) {
	
		startApp("chrome", url);
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, username);
		WebElement elePassword = locateElement("password");
		type(elePassword, password);
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement clickcrm = locateElement("linktext","CRM/SFA");
		click(clickcrm);

		
	}
	@AfterMethod(groups = "all")
	public void close() {
		closeBrowser();
		
	}
	@AfterSuite(groups = "all")
	public void endTest() {
		endResult();
	}
}
