package testCases;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import excel.ReadExcel;
import wdMethods.ProjectMethods;




public class TC001CreateLead extends ProjectMethods{
	
	

	@BeforeTest(groups="all")
		public void setData() {
			testCaseName = "TC001_CreateLead";
			testDesc = "Create A new Lead";
			author = "Alagar";
			category = "smoke";
		}

	//@Test(invocationCount = 2 )
	//@Test(dependsOnMethods = {"testCases.TC002EditLead.editLead"})
	//@Test(timeOut = 15000)
	//@Test(groups = "Smoke")
	@Test(groups = "Smoke" , dataProvider = "qa")
	public void createLead(String CompanyName,String FirstName,String LastName) {
		
		
		WebElement cl = locateElement("linktext","Create Lead");
		click(cl);
		WebElement companyName = locateElement("class", "inputBox");
		type(companyName, CompanyName);
		WebElement firstName = locateElement("id", "createLeadForm_firstName");
		type(firstName, FirstName);
		WebElement lastname = locateElement("createLeadForm_lastName");
		type(lastname, LastName);
		/*WebElement elesource = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingText(elesource, "Conference");*/
		
		WebElement submit = locateElement("class", "smallSubmit");
		click(submit);
			
	}
	
	
	
	@DataProvider(name = "qa" , indices = {1})
	public Object[][] fetchData() throws IOException{
		
		Object[][] data = ReadExcel.readexcel();
		return data;
		
		/*Object[][] data = new Object[2][3];
		data[0][0] = "Renault";
		data[0][1] = "Raju";
		data[0][2] = "S";
		
		
		data[1][0] = "IBM";
		data[1][1] = "Kumar";
		data[1][2] = "V";
		
		return data;*/
		
		
	}
	
	
	
	
	
}