package testCases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC002EditLead extends ProjectMethods{
	
	@BeforeTest(groups = "all")
	public void setData() {
		testCaseName = "TC002_EditLead";
		testDesc = "Edit Lead";
		author = "Alagar";
		category = "smoke";
	}
	
	
	@Test(groups = "Sanity" , dependsOnGroups = {"Smoke"})
	public void editLead() {
		
		WebElement leads = locateElement("linktext", "Leads");
		click(leads);
		
		WebElement find = locateElement("linktext", "Find Leads");
		click(find);
		
		WebElement firstname = locateElement("xpath", "(//input[@name='firstName'])[3]");
		
		type(firstname, "Alagar");
		
		WebElement findleads = locateElement("xpath", "(//button[@class='x-btn-text'])[7]");
		click(findleads);
		
		WebElement selectlead = locateElement("xpath", "(//a[@class='linktext'])[4]");
		click(selectlead);
		
		
		
		WebElement edit = locateElement("xpath", "(//a[@class='subMenuButton'])[3]");
		click(edit);
		
		WebElement companyName = locateElement("class", "inputBox");
		companyName.clear();
		type(companyName, "RNTBCI1");
		WebElement firstName = locateElement("id", "updateLeadForm_firstName");
		firstName.clear();
		type(firstName, "Alagarramanujam");
		WebElement lastname = locateElement("updateLeadForm_lastName");
		lastname.clear();
		type(lastname, "Solai");
		WebElement update = locateElement("xpath", "//input[@value='Update']");
		click(update);
	}
	

}
