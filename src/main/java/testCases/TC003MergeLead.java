package testCases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC003MergeLead extends ProjectMethods{
	
	@BeforeTest(groups = "all")
	public void setData() {
		testCaseName = "TC002_EditLead";
		testDesc = "Edit Lead";
		author = "Alagar";
		category = "smoke";
	}
	
	
	//@Test(groups = "Regression" , dependsOnGroups = {"Sanity"})
	@Test
	public void MergeLeads() {
		
		WebElement leads = locateElement("linktext", "Leads");
		click(leads);
		
		WebElement merge = locateElement("linktext", "Merge Leads");
		click(merge);
		
		WebElement firstlead = locateElement("xpath", "(//img[@alt='Lookup'])[1]");
		click(firstlead);
		
		switchToWindow(1);
		
		WebElement first = locateElement("xpath", "//input[@name='firstName']");
		type(first, "Alagar");
		
		WebElement findl = locateElement("xpath", "(//button[@type='button'])[1]");
		click(findl);
		
		WebElement selectlead = locateElement("xpath", "(//a[@class='linktext'])[1]");
		clickWithoutSnap(selectlead);
		
		
		switchToWindow(0);
		
		WebElement secondlead = locateElement("xpath", "(//img[@alt='Lookup'])[2]");
		click(secondlead);
		
		switchToWindow(1);
		
		WebElement firstname = locateElement("xpath", "//input[@name='firstName']");
		type(firstname, "Alagar");
		
		WebElement find2 = locateElement("xpath", "(//button[@type='button'])[1]");
		click(find2);
		
		WebElement selectlead1 = locateElement("xpath", "(//a[@class='linktext'])[6]");
		clickWithoutSnap(selectlead1);
		
		switchToWindow(0);
		
		WebElement merge1 = locateElement("xpath", "//a[@class='buttonDangerous']");
		clickWithoutSnap(merge1);
		
		
		acceptAlert();
		
	}
	
	

}
